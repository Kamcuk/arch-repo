#!/bin/bash
set -xeuo pipefail
cd "$(dirname "$0")"
out=$(
	{
		cat list.txt
		pacman -Qm | cut -d' ' -f1
	} | sort -u
)
printf "%s\n" "$out" | tee list.txt

