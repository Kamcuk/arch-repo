FROM archlinux/base
MAINTAINER Kamil Cukrowski <kamilcukrowski@gmail.com>
COPY /setup_builder.sh /setup_builder.sh
RUN bash /setup_builder.sh && rm /setup_builder.sh
USER builder
