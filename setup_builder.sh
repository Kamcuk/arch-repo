#!/bin/bash
set -xeuo pipefail

if [ ! -e /.dockerenv ]; then
	echo "error: this script is meant to be in docker" >&2
	exit 2
fi

echo "Installing packages"
pacman-key --init
printf "%s\n" '[archlinuxcn]' 'Server = http://repo.archlinuxcn.org/$arch' >> /etc/pacman.conf
pacman -Sy --noconfirm --needed archlinux-keyring archlinuxcn-keyring
pacman -Suy --noconfirm --needed sudo base-devel git openssh aurutils

echo "setup builder account"
useradd builder --system --shell /sbin/nologin --home-dir /var/cache/build --create-home
passwd -d builder
chmod +w /etc/sudoers
echo "builder ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
chmod -w /etc/sudoers

sudo -u builder bash <<'EOF'
set -x

echo "Disable key checking in ssh config"
mkdir -p ~/.ssh 
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

echo "Use pacman pubring in builder gpg"
mkdir -p ~/.gnupg
chmod 700 ~/.gnupg
echo "keyring /etc/pacman.d/gnupg/pubring.gpg" >> ~/.gnupg/gpg.conf

EOF

sed -i -e 's/^MAKEFLAGS=*/MAKEFLAGS=-j$(nproc)/' /etc/makepkg.conf

echo "Cleanup"
echo -e 'y\ny\n' | pacman -Scc

