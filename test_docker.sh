#!/bin/bash
set -xeuo pipefail

if [[ "$(nice)" = 0 ]]; then
	exec nice ionice "$0" "$@"
fi

if (($#)); then
	docker login registry.gitlab.com
	docker pull registry.gitlab.com/kamcuk/arch-repo:latest ||:
	docker build --cache-from registry.gitlab.com/kamcuk/arch-repo:latest --tag registry.gitlab.com/kamcuk/arch-repo:latest .
	if (($# - 1)); then
		docker push registry.gitlab.com/kamcuk/arch-repo:latest
	fi
fi

IFS=$'\n' files=($(git ls-tree -r master --name-only))
IFS=

docker run -ti --rm \
	-e "KAMILCUKROWSKI_SECRET_GPG_KEY=$(gpg -a --export-secret-keys 8EB01A9F44EC076FDE4838715F64BDEBF73A8E5C)" \
	-e "KAMILCUKROWSKI_OWNERTRUST_GPG=8EB01A9F44EC076FDE4838715F64BDEBF73A8E5C:6:" \
	-v "$(pwd):/this" \
	-w '/this' \
	-v "$(pwd)/public:/repo/public" \
	registry.gitlab.com/kamcuk/arch-repo:latest bash -c '
	set -xeuo pipefail

	sudo mkdir -vp /repo
	sudo cp -vr "$@" /repo
	cd /repo
	sudo chown -R builder .

	./work.sh
	' -- "${files[@]}"

