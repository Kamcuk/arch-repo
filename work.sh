#!/bin/bash
set -xeuo pipefail

# run only in docker #############################################
if [[ ! -e /.dockerenv ]]; then
	echo "error: this script is meant to be in docker" >&2
	exit 2
fi

# envronment ##############################################
export PACKAGER="Kamil Cukrowski via GitLab <kamilcukrowski@gmail.com>"
export AUR_REPO="kamcuk"
dir="$(dirname "$(readlink -f "$0")")"
public="$dir/public"
export AUR_DBROOT="$public/$(uname -m)"
export TMPDIR="$dir/work"
mkdir -p "$AUR_DBROOT" "$TMPDIR"

# SETUP GPG !!!!!!!!!!!!!!! #######################################
cat <<<"${KAMILCUKROWSKI_SECRET_GPG_KEY}" > ./kamilcukrowski-secret-gpg.key
cat <<<"${KAMILCUKROWSKI_OWNERTRUST_GPG}" > ./kamilcukrowski-ownertrust-gpg.txt
export GPGKEY=$(cut -d: -f1 <<<"${KAMILCUKROWSKI_OWNERTRUST_GPG}")
if [[ ! -s ./kamilcukrowski-secret-gpg.key ||
        ! -s ./kamilcukrowski-ownertrust-gpg.txt ]]; then
	rm ./kamilcukrowski-secret-gpg.key ./kamilcukrowski-ownertrust-gpg.txt
	echo "ERROR - keys are missing!" >&2
	exit 2
fi
gpg --import ./kamilcukrowski-secret-gpg.key
gpg --import-ownertrust ./kamilcukrowski-ownertrust-gpg.txt
sudo gpg --homedir /etc/pacman.d/gnupg --import ./kamilcukrowski-secret-gpg.key
sudo gpg --homedir /etc/pacman.d/gnupg --import-ownertrust ./kamilcukrowski-ownertrust-gpg.txt
rm ./kamilcukrowski-secret-gpg.key
# save gpg public key and ownertship 
gpg --armor --export "$GPGKEY" > "$public"/kamilcukrowski-public-gpg.txt
cp "$dir"/kamilcukrowski-ownertrust-gpg.txt "$public"/kamilcukrowski-ownertrust-gpg.txt
# END OF SETUP GPG

# resync public dir #####################################
resync_public_dir() {
	pushd "$public"
	if curl -sS -f -o files.txt https://kamcuk.gitlab.io/arch-repo/files.txt; then
		<files.txt xargs -tr dirname | xargs -tr mkdir -vp
		<files.txt xargs -tr -I{} -P2 curl -sS -f --output {} https://kamcuk.gitlab.io/arch-repo/{}
		# delete all files, will be regenerated - leave only directories
		find . -mindepth 1 -maxdepth 1 -type f -exec rm -v {} +
	fi
	popd # "$publicdir"
}
resync_public_dir

# gpg --recv-keys ####################################
keys=(
	42C9C8D3AF5EA5E3  # compton-conf
	6AD860EED4598027  # zfs
	5C4A26CD4CC8C397  # rdfind
)
gpg --recv-keys "${keys[@]}"

# create repo if it does not exists
if [[ ! -e "$AUR_DBROOT/$AUR_REPO.db.tar" ]]; then
	repo-add -s -k "$GPGKEY" "$AUR_DBROOT/$AUR_REPO.db.tar"
fi

# Add itself repo to pacman
if ! grep -F -x -q "[$AUR_REPO]" /etc/pacman.conf; then 
	printf "%s\n" "[$AUR_REPO]" "Server = file://$public/\$arch" |
		sudo tee -a /etc/pacman.conf
	sudo pacman -Sy
fi

# fix missing dependencies on python-pyelftools
sudo pacman -S --needed --noconfirm python2-setuptools python-setuptools

# AUR_REPO and AUR_DBROOT and TMPDIR environment variables are used
<list.txt sed '/[[:space:]]*#/d; /^[[:space:]]*$/d' |
	xargs -t -d $'\n' aur sync -n -u --noview --sign

# remove old database
find "$public" -type f '(' \
	   -name '*.files.tar.xz.old' \
	-o -name '*.files.tar.xz.old.sig' \
	-o -name '*.db.tar.xz.old' \
	-o -name '*.db.tar.xz.old.sig' \
	')' -exec rm -v '{}' +

# create files.txt
( cd "$public" && find . -type f -mindepth 1 ) | sed 's@./@@' | sort | tee "$public"/files.txt
if sed -n '/[^[:alnum:][:punct:]]/{p;q};$q1' "$public"/files.txt; then
	echo "ERROR: there is a unprintable filename in files.txt!" >&2
	exit 2
fi

# generate index.html
tmp=$(mktemp)
trap 'rm -r "$tmp"' EXIT
( cd "$public" && <"$public"/files.txt xargs stat -c '%Y %n') |
sort -k1rn -k2 |
awk '{  
	printf "<tr><td style=\"text-align:left\">\n\t"
	printf strftime("%Y-%m-%dT%H:%M:%S%z", $1, 1)
	printf "</td><td>\n\t"
	printf "<a href=\"" $2 "\">" $2 "</a>"
	printf "</td></tr>\n"
}' > "$tmp"
sed -e '/@@FILES@@/{ s///; r '"$tmp" -e '}' ./index_template.html > "$public"/index.html
cat "$public"/index.html

echo "DONE"
